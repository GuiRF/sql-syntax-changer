# -*- coding: cp1252 -*-
import time
import re
import sys, os
import shutil
import Tkinter
import tkFileDialog

def begin():
    os.system("cls")
    timedata = time.time()

    if not len(sys.argv) == 2:
        print "[SYS] Open file request.."
        _root = Tkinter.Tk()
        _root.withdraw()
        name = tkFileDialog.askopenfilename(parent=_root)
        _root.quit()
    else:
        print "[SYS] File name received.."
        name = sys.argv[1]

    print "[SYS] Opening file.."
    rf = open(name, "r")
    data = rf.read()
    rf.close()

    newdata = ""

    thetype = input("SQLite > MySQL or MySQL > SQLite? [1/2]: ")

    print "\n[SYS] Parsing file data.."
    for line in data.split("\n"):
        if re.search("self.query.execute", line):

            line = (line.replace("?", "%s") if thetype == 1 else line.replace("%s", "?"))
            
            n = (re.search(", ?\[(.+)\]", line) if thetype == 1 else re.search(", ?\((.+)\)", line))
            if n != None:
                n = n.group()
                old = n
                n = (n.replace("[", "(", 1).replace("]", ")", 1) if thetype == 1 else n.replace("(", "[", 1).replace(")", "]", 1))
                
                line = line[:len(old)*-1] + (n[1:] + ")" if thetype == 1 else n)
                shutil.copy(name, name[:-3] + " - " + ("SQLite" if thetype == 1 else "MySQL") + name[-3:])
        newdata += line + "\n"

    print "[SYS] Replacing file data.."
    wf = open(name, "w")
    wf.write(newdata)
    wf.close()

    t = time.time() - timedata
    t = str("%1f" %(t))[:-3]
    print "[SYS] Done. Taken time: %ss!" %(t)

    raw_input("<>: ")
    sys.argv = sys.argv[0]
    begin()
begin()